<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class TrackResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $track_artist = (!empty( $this->track_artist)) ? $this->track_artist :
            ((!empty($this->artist)) ? $this->artist->artist_display_name : '');
        return [
            'id' => $this->track_id,
            'api_path' => '/track/' . $this->track_id,
            'disc_num' => $this->track_disc_num,
            'track_num' => $this->track_track_num,
            'song_title' => (!empty($this->song->song_title)) ? $this->song->song_title : 'TBD',
            'display_title' => $this->track_title,
            'track_artist' => $track_artist,
            'track_artist_alias' => (!empty($this->artist->artist_alias)) ? $this->artist->artist_alias : '',
            'alias' => $this->track_alias,
            'visible' => $this->track_is_visible,
            'release' => $this->release->release_catalog_num,
            'album' => $this->release->album->album_title,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
        ];
    }
}
